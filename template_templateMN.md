---
author: Aleksander Burnat
title: Prezentacja o Diunie
subtitle: Książce Franka Herberta
date:  07.11.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Wygląd okładki
![Okładka Diuny](220px-Dune-Frank_Herbert_(1965)_First_edition.webp)

**Tak właśnie wyglądało wydanie diuny (pierwsze) z 1965 roku**


## Saga Diuny

<!--- UWAGA WAŻNE pusta linia odstępu -->

_Do książek napisanych przez Franka Herberta zaliczają się_:
1. Diuna
2. Mesjasz Diuny
3. Dzieci Diuny
4. Bóg Imperator Diuny
5. Heretycy Diuny
6. Kapitularz Diuną

~~Do książek napisanych przez Briana Herberta i innych zaliczają się~~:

* Dżihad Butleriański (2002) 
* Krucjata przeciw maszynom (2003)
* Bitwa pod Corrinem (2004)

Następnie powstały kolejne książki o diunie natomiast ich nie czytałem i nie jestem w stanie sie odnieść do nich i na ile są zgodne z uniwersum.
## Filmy

### Diuna – film 1984 
![Okładka](https://upload.wikimedia.org/wikipedia/en/thumb/5/51/Dune_1984_Poster.jpg/220px-Dune_1984_Poster.jpg)

### Miniserial Diuna (2000)
![](https://upload.wikimedia.org/wikipedia/en/thumb/8/85/Dune-miniseries.jpg/250px-Dune-miniseries.jpg)

### Dzieci Diuny (2003)
Sequel do miniserialu diuna
![](https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/Children_of_Dune_1.jpg/250px-Children_of_Dune_1.jpg)

### Ostatni Film Diuna (2021)
![](https://upload.wikimedia.org/wikipedia/en/thumb/8/8e/Dune_%282021_film%29.jpg/220px-Dune_%282021_film%29.jpg)
<!--- Niestety pomiędzy tak zdefiniowanymi  blokami nie można umieszczać innych elementów-->

## Frank Herbert

\begin{alertblock}{Uwaga:}
Żył pomiędzy 8 października 1920 a 11 lutego 1986
\end{alertblock}

Twórczość Herberta określana jest jako soft science fiction i łączy zagadnienia z zakresu ekologii, filozofii, teologii, psychologii i ekonomii. Herbert koncentruje się na naturze człowieka, na jego możliwościach i niebezpieczeństwach, które mu grożą ze strony wymykającej się spod kontroli techniki, systemów totalitarnych i narkotyków zmieniających stan świadomości. Inspirowały go religie: buddyzm, którego był wyznawcą, a także islam. Pisarstwo Herberta bardziej skupia się na przyszłości samego człowieka niż tworzonych przez niego technologii.

## Inne Powieści tego autora


